<?php

include 'config/call.php';
$id=$_GET['id'];
try
     {
      $stmt = $conn->prepare("SELECT * FROM users where id='$id'");
      $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_ASSOC); 
      $result=$stmt->fetch();
      
     }
     catch(PDOException $e)
     {?>
      <script>
        alert("Error while fetching data");
      </script>
     <?php
     }



?>






<html>
<head>
    <title>User Crud</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <h1>Add User</h1>

        <form action="update.php" class="form" id="signupform" role="form" method="post">
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" value=" <?php echo  $result['email']; ?>" id="email" name="email" required>
            </div>
            <div class="form-group">
                <label for="first_name">First Name</label>
                <input type="text" class="form-control" value=" <?php echo $result['first_name'] ?>" id="first_name" name="first_name" required>
            </div>
            <div class="form-group">
                <label for="last_name">Last Name</label>
                <input type="text" class="form-control" value=" <?php echo $result['last_name'] ?>" id="last_name" name="last_name" required>
            </div>
            <div class="form-group">
                <input type="hidden" class="form-control" value=" <?php echo  $result['id']; ?>" id="id" name="id">
                <button class="btn btn-success">Save</button> 
                  <button href="list.php" class="btn btn-primary">Back</button> 
            </div>
        </form>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


<script type="text/javascript">
  $('#signupform').submit(function() {
    /*alert("Hello");*/
  var filter = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  var number= /[0-9 -()+]+$/;
  var alpha= /^[a-zA-Z0-9!-”$%&’()*\+,\/;\[\\\]\/\s^_.`{|}~]+$/;  
  var phone_no=/^(?:\+\d{2})?\d{10}(?:,(?:\+\d{2})?\d{10})*$/;


 var symbols = /[-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/]/;
  var firstname=$('#first_name').val();

  /*alert(middlename);*/
  var lastname=$('#last_name').val();
  /*alert(lastname);*/
 
  var admin =$('#admin').val();
  var gender =$('#gender').val();


  if(!alpha.test(firstname))
  {
    /*alert(firstname);*/
    $("#first_name").css({"border": "1px solid red"});
    $('#first_name').focus();
    setTimeout(function() {
       $('#first_name').css({"border": "1px solid #ddd"});
   }, 3000);
        return false;
  }
 
 if(symbols.test(firstname))
  {
    /*alert(firstname);*/
    $("#first_name").css({"border": "1px solid red"});
    $('#first_name').focus();
    setTimeout(function() {
       $('#first_name').css({"border": "1px solid #ddd"});
   }, 3000);
        return false;
  }

if(number.test(firstname))
  {
    /*alert(firstname);*/
    $("#first_name").css({"border": "1px solid red"});
    $('#first_name').focus();
    setTimeout(function() {
       $('#first_name').css({"border": "1px solid #ddd"});
   }, 3000);
        return false;
  }
   if(symbols.test(firstname))
  {
    /*alert(firstname);*/
    $("#first_name").css({"border": "1px solid red"});
    $('#first_name').focus();
    setTimeout(function() {
       $('#first_name').css({"border": "1px solid #ddd"});
   }, 3000);
        return false;
  }



   if(!alpha.test(lastname))
  {
    /*alert(firstname);*/
    $("#last_name").css({"border": "1px solid red"});
    $('#last_name').focus();
    setTimeout(function() {
       $('#last_name').css({"border": "1px solid #ddd"});
   }, 3000);
        return false;
  }
   if(number.test(lastname))
  {
    /*alert(firstname);*/
    $("#last_name").css({"border": "1px solid red"});
    $('#last_name').focus();
    setTimeout(function() {
       $('#last_name').css({"border": "1px solid #ddd"});
   }, 3000);
        return false;
  }

  else 
  {
    return true;
  }
});
        </script>
</body>
</html>
