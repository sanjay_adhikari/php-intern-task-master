<html>
<head>
    <title>User Crud</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <h1>Add User</h1>
        <form action="add.php" class="form" role="form" id="signupform" method="post">
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" name="email" required>
            </div>
            <div class="form-group">
                <label for="first_name">First Name</label>
                <input type="text" class="form-control" id="first_name" value="" name="first_name" required >
            </div>
            <div class="form-group">
                <label for="last_name">Last Name</label>
                <input type="text" class="form-control" id="last_name" name="last_name" required>
            </div>
            <div class="form-group">
                <input type="submit" name="save" class="btn btn-success">
          
            </div>
        </form>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


<script type="text/javascript">
  $('#signupform').submit(function() {
    /*alert("Hello");*/
  var filter = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  var number= /[0-9 -()+]+$/;
  var alpha= /^[a-zA-Z0-9!-”$%&’()*\+,\/;\[\\\]\/\s^_.`{|}~]+$/;  
  var phone_no=/^(?:\+\d{2})?\d{10}(?:,(?:\+\d{2})?\d{10})*$/;


 var symbols = /[-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/]/;
  var firstname=$('#first_name').val();

  /*alert(middlename);*/
  var lastname=$('#last_name').val();
  /*alert(lastname);*/
 
  var admin =$('#admin').val();
  var gender =$('#gender').val();


  if(!alpha.test(firstname))
  {
    /*alert(firstname);*/
    $("#first_name").css({"border": "1px solid red"});
    $('#first_name').focus();
    setTimeout(function() {
       $('#first_name').css({"border": "1px solid #ddd"});
   }, 3000);
        return false;
  }
 
 if(symbols.test(firstname))
  {
    /*alert(firstname);*/
    $("#first_name").css({"border": "1px solid red"});
    $('#first_name').focus();
    setTimeout(function() {
       $('#first_name').css({"border": "1px solid #ddd"});
   }, 3000);
        return false;
  }

if(number.test(firstname))
  {
    /*alert(firstname);*/
    $("#first_name").css({"border": "1px solid red"});
    $('#first_name').focus();
    setTimeout(function() {
       $('#first_name').css({"border": "1px solid #ddd"});
   }, 3000);
        return false;
  }
   if(symbols.test(firstname))
  {
    /*alert(firstname);*/
    $("#first_name").css({"border": "1px solid red"});
    $('#first_name').focus();
    setTimeout(function() {
       $('#first_name').css({"border": "1px solid #ddd"});
   }, 3000);
        return false;
  }



   if(!alpha.test(lastname))
  {
    /*alert(firstname);*/
    $("#last_name").css({"border": "1px solid red"});
    $('#last_name').focus();
    setTimeout(function() {
       $('#last_name').css({"border": "1px solid #ddd"});
   }, 3000);
        return false;
  }
   if(number.test(lastname))
  {
    /*alert(firstname);*/
    $("#last_name").css({"border": "1px solid red"});
    $('#last_name').focus();
    setTimeout(function() {
       $('#last_name').css({"border": "1px solid #ddd"});
   }, 3000);
        return false;
  }

  else 
  {
    return true;
  }
});
        </script>
</body>
</html>
