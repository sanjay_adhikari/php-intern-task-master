<?php

namespace Classes;

class User extends DB
{
    private $firstName;
    private $lastName;
    private $email;
    private $id;

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }
 
    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }/**
     * @return mixed
     */
   public function getid()
    {
        return $this->id;
    }
 /**
     * @param mixed $id
     */
     public function setid($id)
    {
        $this->id = $id;
    }




    public function insert()
    {

        $query = "insert into users(email,first_name, last_name) 
            values(
            '".$this->getEmail()."',
            '".$this->getFirstName()."',
            '".$this->getLastName()."'
            )";

        return mysqli_query($this->db, $query);
    }

 public function update()
    {

$id=$this->getid();
$email=$this->getEmail();
$firstName=$this->getFirstName();
$lastName=$this->getLastName();
echo $id;

        $query = "UPDATE users SET email='$email',first_name='$firstName',last_name='$lastName' where id='$id'";
          
          return mysqli_query($this->db, $query);
      
            }
        
             
           
        
            


    public function delete()
    {

  $id=$this->getid();
        $query = "DELETE FROM `users` WHERE id='$id'";
          return mysqli_query($this->db, $query);
          
    
    }

    


    public function getAll()
    {
        $query = 'select * from users';
        $result = mysqli_query($this->db, $query);
        $users = [];
        if (mysqli_num_rows($result) > 0) {
            while ($user = mysqli_fetch_assoc($result)) {
                $thisUser = new self();
                $thisUser->setFirstName($user['first_name']);
                $thisUser->setLastName($user['last_name']);
                $thisUser->setEmail($user['email']);
                  $thisUser->setid($user['id']);



                $users[] = $thisUser;
            }
        }

        return $users;
    }
}
