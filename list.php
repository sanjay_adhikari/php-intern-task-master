<?php

include 'Classes/DB.php';
include 'Classes/User.php';
$users = (new Classes\User())->getAll();

?>

<html>
<head>
    <title>User Crud</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <h1>User List</h1>

           <div width="100%" align="right" height="">
                                        <a href="index.php">
                                            <button class="btn btn-success btn-lg">
                                            <i  class="fa fa-user-plus" ></i>
                                            Add
                                            </button>
                                        </a>
                                    </div>

                  
        <table class="table">
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th></th>
            </tr>
            <?php foreach ($users as $user): ?>
                <tr>
                   
                    <td><?php echo $user->getFirstName(); ?></td>
                    <td><?php echo $user->getLastName(); ?></td>
                    <td><?php echo $user->getEmail(); ?></td>
                    <td>
                         <a href="edit.php?id=<?php echo $user->getid(); ?>">
                            <button type="submit" class="btn btn-primary btn-xs">
                               
                                EDIT
                            </button>
                             <a href="delete.php?id=<?php echo $user->getid(); ?>"onclick="return confirm('Are you sure to delete?');">
                            <button type="submit" class="btn btn-danger btn-xs">
                               
                                Delete
                            </button>



                   </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>






</body>
</html>
